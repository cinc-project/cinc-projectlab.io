---
title: "Free products"
authors: ["tensibai"]
draft: false
date: 2023-11-02T21:48:55-04:00
tags:
  - Cinc
  - Client
  - Server
  - Auditor
  - Workstation
  - Dashboard
---

Here’s the short list of changes people migrating solutions can expect to look into (updated 2023-11-02):

- Chef Infra™ client –> Cinc-client
- Chef Infra™ server –> Cinc-server or Goiardi
- Chef Inspec™ –> Cinc-auditor
- Chef Habitat™ –> see Cinc-project friends biome.sh
  - Cinc’s own Habitat distro is considered low priority by the project in the presence of such a great alternative.
- Chef Workstation –> Cinc-Workstation
- Chef Automate -> Looking for contributors to help building it.
  - Alternative to get an overview of a cinc server: [chefbrowser](https://github.com/drewhammond/chefbrowser)
