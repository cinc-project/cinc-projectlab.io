---
title: "April 2020 update"
authors: ["bobchaos"]
date: 2020-04-03T21:48:55-04:00
draft: false
tags:
  - Cinc
  - Client
  - Server
  - Auditor
  - Workstation
  - Project_Team
---

## What's new with Cinc?

We've been busy! Since my last [post in january]({{< relref "cooking_with_cinc.md" >}}), the team has not only kept up with releases of Cinc-Client and Cinc-Auditor but also improved many aspects of the project.

With the release of Chef Infra 16 approaching fast, we've started getting Cinc-Client 16 builds ready. There's also been significant work on our Cinc-Workstation pipeline, which now outputs artifacts of all but Windows builds, and those are in progress too.

Our pipelines have been significantly refactored to simplify the way we manage our patching process and allow easily embedding our gems into otherwise standard Omnibus packages. We now use a `git merge` based strategy as opposed to maintaining patch files. If you were maintaining your own distribution based on our work you'll want to have a look at the new format for our pipelines.

You may also notice some improvements on this very website: We have the first implementation of our RSS feed that went live recently. We're interested in feedback on the format and content of the feed. Another less noticeable change is the addition of a robots.txt and sitemap.

You may also be interested to know that work has begun on Cinc-Server. While there isn't much more to report on that topic yet we have a very motivated contributor who's picked it up and is actively working on it.

In related news, Cinc-project friend and contributor CT has been hard at work on his own implementation of the Chef Infra&trade; server API, [Goiardi](http://goiardi.gl), in order to address some of the last missing pieces for full Chef Infra&trade; server 13 compliance.

## Contributors new and old

Project contributors have changed a bit these past months. Most notably, one of our founding members, Artem Sidorenko, has left the project to pursue some exciting life opportunities. Artem is the man behind Cinc-Auditor, for which he both did the work upstream and created the build pipelines. We can't thank you enough for your deep involvement in the Cinc Project and we wish you all the best!

We also have a few new faces. We'd like to welcome Vern Burton (tarcinil). He brings some much needed Golang skills to the project team, skills that he's already been putting to good use in the repos that constitute the Workstation. He's been working at implementing the [Go code generator](https://github.com/chef/go-libs/tree/master/distgen) created by Salim Afiune in relevant Workstation projects. Thanks to his contributions we're confident our first release of Cinc-Workstation is just around the corner. Welcome aboard!

Just arrived in the Chef Community a few weeks ago, we have Josh Gitlin (jgitlin), who's wasted no time and started work on Cinc-Server roughly 5 minutes about hearing about Cinc! Such enthusiasm is more than welcome and we look forward to celebrating the first PRs against the Chef Server repos to implement configurable distribution names. Welcome aboard!

We're also seeing an increase in traffic on our [Gitlab](https://gitlab.com/cinc-project), with small but helpful contributions such as opening issues, providing feedback, helping keep docs up to date, and correcting my horrible grammar and vocabulary on this very blog o.O Free Open Source Software projects live by such small contributions and we encourage everyone reading this to pitch in however they can. Visit the [contributing page]({{< ref "page/contributing.md" >}}) for some ideas of how you can help too.

## Chef free products EOL

We knew the day was coming, now it's just around the corner: Unless there's a drastic turn of events, on May 1st 2020 most of Chef's free binaries will have reached end of life and will no longer be receiving updates from Chef Software. This will leave a lot of folks with 2 options: get a commercial agreement with Chef Software, or move to other solutions.

Cinc being the exact same code, barring trademark related changes, you can confidently migrate to our binaries. Existing automation should continue functioning thanks to our wrappers around the upstream binaries, and so will your muscle memory if you've been typing `chef-client` for a decade. If this statement proves to be inaccurate please open an issue, we'll do our best to address it.

Here's the short list of changes people migrating solutions can expect to look into:

- Chef Infra&trade; client --> Cinc-client
- Chef Infra&trade; server --> [Goiardi](http://goiardi.gl)
  - Cinc-server is being worked on
  - Chef Server 12 is not actually going EOL yet
- Chef Inspec&trade; --> Cinc-auditor
- Chef Habitat&trade; --> see Cinc-project friends [biome.sh](https://biome.sh)
  - Cinc's own Habitat distro is considered low priority by the project in the presence of such a great alternative.
- Chef Workstation --> Cinc-Workstation
  - Experimental builds are available, but currently not compliant with Chef's trademark policy
    - Windows builds have proven to be more difficult and are not currently available, but we're working on it.
  - Under the Chef EULA, you may still use our workstation for testing purposes, and we'd certainly welcome the feedback!
  - We're looking for someone with expertise in Rust to contribute. If you want to help come see us on Gitlab or the Chef Community Slack.

## That's some big updates

Absolutely! As I write this a significant proportion of the world population is in self-imposed quarantine including project members. I can only assume it will take it's toll on our own working pace, but we're not stopping on account of some pandemic. We still hope to have Cinc-Workstation ready for May 1st so no one gets stuck with EOL software.

This concludes this update. Try out the new RSS feed to get future updates delivered to you. 
Take care of yourselves and each other, especially in these strange times.
